//mass delete products by id

let deleteButton = document.getElementById("delete");


//get checked products
function getCheckedValues() {
    return Array.from(document.querySelectorAll(".delete-checkbox"))
        .filter((checkbox) => checkbox.checked)
        .map((checkbox) => checkbox.parentElement.getAttribute("data-id"))
}

//delete products
if(deleteButton){
    deleteButton.addEventListener("click", () => {
        const ids =  getCheckedValues().join(",");
        const url = "http://scandiwebtestapi.junelike.ru/data/delete.php";

        fetch(url, {
            method : "POST",
            body: JSON.stringify({ ids: ids }),
        }).then(
            response => response.text()
        ).then(
            html => document.location.reload()
        );

    });
}