//check before create product

let checkSKU = document.getElementById("sku");
let productItem = document.getElementById("productType");
let inputs = document.querySelectorAll(".product__input-data");
let price = document.getElementById("price");

//check if SKU unique
if(checkSKU){
    checkSKU.addEventListener("input", (e) => {
        const sku = checkSKU.value;
        const url = "http://scandiwebtestapi.junelike.ru/data/check.php";

        if(sku != ""){
            fetch(url, {
                method : "POST",
                body: JSON.stringify({ sku: sku }),
            }).then(
                response => response.text()
            ).then(
                html => e.target.parentElement.querySelector(".product__notification").innerHTML = html
            );
        } else {
            this.parentElement.querySelector(".product__notification").innerHTML = "";
        }


    });
}

//get values by type
if(productItem){
    productItem.addEventListener("change", (e) => {
        let selectedTypeValue = e.target.value;

        document.querySelectorAll('.product__switcher-content-item').forEach(function(item) {
            item.style.display = "none";
        });

        if(selectedTypeValue != "type"){
            document.getElementById(selectedTypeValue).style.display = "block";
            let value= "";
            let measuring = 0;
            switch (selectedTypeValue){
                case "dvd":
                    value = document.getElementById("size").value;
                    measuring = document.getElementById("dvd").dataset.measuring;
                    break;
                case "furniture":
                    value = document.getElementById("height").value + "x" + document.getElementById("width").value + "x" + document.getElementById("length").value;
                    measuring = document.getElementById("furniture").dataset.measuring;
                    break;
                case "book":
                    value = document.getElementById("weight").value;
                    measuring = document.getElementById("book").dataset.measuring;
                    break;
                default:
                    break;
            }
        }

    });
}

//check data for type on input
Array.from(inputs).map(item => {
    item.oninput = (e) => {
        if(!/^[0-9]+$/.test(item.value)){
            item.parentElement.querySelector(".product__notification").innerHTML = "Please, provide the data of indicated type";
        }
        else{
            item.parentElement.querySelector(".product__notification").innerHTML = "";
        }

    }
});

//check price
if(price){
    price.addEventListener("input", (e) => {
        if(!/^[0-9\.]+$/.test(e.target.value)){
            e.target.parentElement.querySelector(".product__notification").innerHTML = "Please, provide the data of indicated type";
        }
        else{
            e.target.parentElement.querySelector(".product__notification").innerHTML = "";
        }
    });
}

