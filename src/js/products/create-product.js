//create product

let saveButton = document.getElementById("save");
let inputs = document.querySelectorAll(".product__input");
let inputsData = document.querySelectorAll(".product__input-data");
let productItem = document.getElementById("productType");


//change type name before create product
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


//create product
if(saveButton){
    saveButton.addEventListener("click", () => {

        let validation = true;

        Array.from(inputs)
            .filter(item => item.value === "")
            .map(item => {
                item.parentElement.querySelector(".product__notification").innerHTML = "Please, submit required data";
                validation = false;
            });

        switch (productItem.value){
            case "dvd":
                document.getElementById("productSwitcher").setAttribute("data-value",document.getElementById("size").value);
                document.getElementById("productSwitcher").setAttribute("data-measuring",document.getElementById("dvd").dataset.measuring);
                break;
            case "furniture":
                document.getElementById("productSwitcher").setAttribute("data-value",document.getElementById("height").value + "x" + document.getElementById("width").value + "x" + document.getElementById("length").value);
                document.getElementById("productSwitcher").setAttribute("data-measuring",document.getElementById("furniture").dataset.measuring);
                break;
            case "book":
                document.getElementById("productSwitcher").setAttribute("data-value",document.getElementById("weight").value);
                document.getElementById("productSwitcher").setAttribute("data-measuring",document.getElementById("book").dataset.measuring);
                break;
            default:
                break;
        }

        const sku = document.getElementById("sku").value;
        const name = document.getElementById("name").value;
        const price = document.getElementById("price").value;
        const type_name = capitalizeFirstLetter(document.getElementById("productType").value);
        const type_value = document.getElementById("productSwitcher").dataset.value;
        const type_measuring = document.getElementById("productSwitcher").dataset.measuring;
        const url = "http://scandiwebtestapi.junelike.ru/data/create.php";

        if(validation){
            fetch(url, {
                method : "POST",
                body: JSON.stringify(
                    {
                        sku: sku,
                        name: name,
                        price: price,
                        type_name: type_name,
                        type_value: type_value,
                        type_measuring: type_measuring
                    }),
            }).then(
                response => response.text()
            ).then(
                html => window.location.href = "index.html"
            );
        }


    });
}