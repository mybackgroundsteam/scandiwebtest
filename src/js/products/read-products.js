//read and view products

let productsContainer = document.getElementById("productsList");


function showProducts(data){

    data.records.forEach(function (item) {
        let product =
            `<div class="products__item" data-id="${item.id}">
        <input type="checkbox" class="delete-checkbox" name="delete"/>
        <span class="products__item-scu">${item.sku}</span>
        <span class="products__item-name">${item.name}</span>
        <span class="products__item-price">${item.price} $</span>
        <div class="products__item-attributes">
            <span class="products__item-attributes-name">${item.type_name}:</span>
            <span class="products__item-attributes-content">${item.type_value} ${item.type_measuring}</span>
        </div>
    </div>`;

        productsContainer.insertAdjacentHTML("beforeend", product);

    });
}

async function loadData() {
    const response = await fetch("http://scandiwebtestapi.junelike.ru/data/read.php");
    const products = await response.json();

    showProducts(products);
}

if(productsContainer){
    loadData();
}










